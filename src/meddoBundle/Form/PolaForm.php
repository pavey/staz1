<?php

namespace meddoBundle\Form;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class PolaForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('miasto');
        $builder->add('specjalizacja');
    }
    public function getName() {
        return 'pola';
    }
}