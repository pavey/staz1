<?php

namespace meddoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use meddoBundle\Entity\Pola;
use meddoBundle\Form\PolaForm;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
class DefaultController extends Controller {

    public function indexAction(Request $request) {
        $pola = new Pola();
        $form = $this->createForm(new PolaForm(), $pola);
        $form->handleRequest($request);

        if ($form->isValid()) {

            return $this->forward('meddoBundle:Default:wynik', array('miasto' => $pola->getMiasto(), 'specjalizacja' => $pola->getSpecjalizacja()));
        }
        return $this->render('meddoBundle:Default:index.html.twig', array('form' => $form->createView()));
    }

    public function wynikAction($miasto, $specjalizacja) {

        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
                        'SELECT p
    FROM meddoBundle:Users p
    WHERE p.imie = :miasto
    AND p.nazwisko = :specjalizacja'
                )->setParameter('miasto', $miasto)
                ->setParameter('specjalizacja', $specjalizacja);

        $dane = $query->getResult();
        if(empty($dane)){
            return new Response('Nie znaleziono rekordu w bazie o parametrach: Miasto: '.$miasto.' oraz Specjalizacja: '. $specjalizacja.'.');
        }

        return $this->render('meddoBundle:Default:wyniki.html.twig',array('dane'=>$dane));
    }
    
    public function stylesheetAction(Request $request)
    {
        return $this->render('meddoBundle:Default:stylesheet.html.twig');
    }

}
