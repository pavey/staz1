<?php

namespace meddoBundle\Entity;
class Pola {
    protected $miasto;
    protected $specjalizacja;
    
    public function getMiasto(){
        return $this->miasto;
    }
    public function getSpecjalizacja(){
        return $this->specjalizacja;
    }
    public function setMiasto($miasto){
        $this->miasto = $miasto;
    }
    public function setSpecjalizacja($specjalizacja){
        $this->specjalizacja = $specjalizacja;
    }
}